# Minds Status

This repository hosts the [status.minds.com](https://status.minds.com/) site. 

## Creating an incident

- `npm run new-incident`
- Create a new issue with the ~"Type::Incident" label

## Resolving an issue

- `npm run update-incident`

```
::: update Resolved | 2019-08-16T01:24:45.752Z
Place some text here to say that the issue has been resolved.
:::
```
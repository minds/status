module.exports = {
  "title": "Minds Status",
  "name": "minds_status",
  "description": "Minds Status Site",
  "defaultLocale": "en",
  "locales": [
    {
      "code": "en",
      "iso": "en-US",
      "name": "English",
      "file": "en.json"
    }
  ],
  "content": {
    "frontMatterFormat": "json",
    "systems": [
      'web',
      'api',
      'boost',
      'comments',
      'groups',
      'live-updates',
      'messenger',
      'newsfeed',
      'notifications',
      'search',
      'video-chat',
      'videos',
      'wallet',
      'wire',
    ],
  },
  head: {
    link: [
      { rel: 'mask-icon', href: '/img/logo.svg', color: '#3e4e88' },
    ],
  },
}
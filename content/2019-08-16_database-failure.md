---json
{
  "title": "Database failure",
  "date": "2019-08-16T16:45:04.598Z",
  "severity": "major-outage",
  "affectedsystems": [
    "web"
  ],
  "resolved": true,
  "modified": "2019-08-16T16:50:04.598Z"
}
---

Incident Issue: [https://gitlab.com/minds/minds/issues/746](https://gitlab.com/minds/minds/issues/746).

::: update Resolved | 2019-08-16T16:50:04.598Z
Brief outage due to subsequent database failures caused by a repair to the `boosts` table. This has now been resolved and is being monitored.

Some users are reporting issues with their profiles, if you are also having problems then please visit the Help & Support group. This issue is being track via [engine#754](https://gitlab.com/minds/engine/issues/754).
:::

<!--- language code: en -->

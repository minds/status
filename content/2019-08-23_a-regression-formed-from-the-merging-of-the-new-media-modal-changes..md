---json
{
  "title": "Outages for iOS / Safari",
  "date": "2019-08-23T17:49:22.606Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "web"
  ],
  "resolved": true,
  "modified": "2019-08-16T19:00:45.598Z"
}
---
We have received reports of the inability to play videos on iOS and Safari and are currently working on the resolution.

::: update Update | 2019-08-23T18:22:45.752Z
This issue is being tracked via [font!1799](https://gitlab.com/minds/front/issues/1799). 
:::

::: update Resolved | 2019-08-23T19:00:45.752Z
This issue has been resolved and we will continue to monitor via [font!1799](https://gitlab.com/minds/front/issues/1799).
:::

<!--- language code: en -->

---json
{
  "title": "Old messages not appearing for multiple users.",
  "date": "2019-08-27T17:40:34.666Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "messenger"
  ],
  "resolved": true,
  "modified": "2019-08-27T18:55:58.349Z"
}
---
We are investigating reports that some users are missing their message history.

::: update Update | 2019-08-23T18:22:45.752Z
This issue was tracked via [front!1810](https://gitlab.com/minds/front/issues/1810).

This incident has been resolved, and should not reoccur.
:::


<!--- language code: en -->

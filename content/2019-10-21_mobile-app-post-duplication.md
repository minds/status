---json
{
  "title": "Mobile app post duplication",
  "date": "2019-10-21T19:30:57.437Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "newsfeed"
  ],
  "resolved": true,
  "modified": "2019-10-21T23:22:02.768Z"
}
---

This issue is being tracked via [engine#1030](https://gitlab.com/minds/engine/issues/1030).

We are investigating an incident wherein a users posts is being spmmed across mobile feeds, in some instances causing crashes.

::: update Update | 2019-10-21T02:24:47.752Z
This issue has been resolved and we will be working on a patch to enure it does not happen again.
<!--- language code: en -->

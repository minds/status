---json
{
  "title": "Load balancing issue in Canary",
  "date": "2019-10-31T01:30:38.984Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "web"
  ],
  "resolved": true,
  "modified": "2019-10-31T16:23:26.517Z"
}
---
Canary users experienced an outage when the assosciated container went down due to resource misallocation. This was quickly picked up by the team, and is now resolved. 

Adjustments have been made and we are monitoring the containers to ensure it doesn't happen again.

::: update Update | 2019-10-31T16:10:22.102Z
We're investigating another occurance of the Canary container going down.
If you're experiencing this issue, please clear your cookies, and ensure you are not in Canary.
:::
<!--- language code: en -->

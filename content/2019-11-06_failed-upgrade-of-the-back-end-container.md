---json
{
  "title": "Failed upgrade of the back-end container",
  "date": "2019-11-06T13:17:48.033Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "web"
  ],
  "resolved": true 
}
---
We believe that an upgrade of a backend container has failed causing an outage for some users. We will update with further information.
We are tracking this issue via [engine#1098](https://gitlab.com/minds/engine/issues/1098)


::: update Resolved | 2019-11-06T13:48:01.528Z
We believe this issue to be resolved, but will continue to monitor.
:::

<!--- language code: en -->

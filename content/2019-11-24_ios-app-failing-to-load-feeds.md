---json
{
"title": "Mobile authentication issues",
"date": "2019-11-24T01:43:49.760Z",
"severity": "partial-outage",
"affectedsystems": [
"api"
],
"resolved": true,
"modified": "2019-11-24T11:11:58.057Z"
}

---

We are investigating multiple reports of connectivity issues with the mobile apps. This issue it being tracked via [minds#962](https://gitlab.com/minds/minds/issues/962).

::: update Resolved | 2019-11-24T11:11:58.057Z
The issue was caused due to OAuth refresh tokens not being triggered due to a backend status code change. A [patch (cd45b302)](https://gitlab.com/minds/engine/commit/cd45b3023734a359c09812dbc57fb1ad25dbafb1) has been deployed to fix the issue.
:::

<!--- language code: en -->

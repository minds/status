---json
{
  "title": "Intermittent 502 errors",
  "date": "2020-02-10T20:58:33.292Z",
  "severity": "degraded-performance",
  "affectedsystems": [
    "web"
  ],
  "resolved": true,
  "modified": "2020-02-11T01:28:50.026Z"
}
---
We are experiecing memory leaks on our frontend servers which are triggering 502 errors some users. We are tracking this issue at https://gitlab.com/minds/front/issues/2545.

<!--- language code: en -->

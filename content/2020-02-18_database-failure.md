---json
{
  "title": "Database Failure",
  "date": "2020-02-18T12:57:43.889Z",
  "severity": "major-outage",
  "affectedsystems": [
    "web",
    "api",
    "boost",
    "groups",
    "newsfeed",
    "search",
    "videos",
    "wallet"
  ],
  "resolved": true,
  "modified": "2020-02-18T13:13:45.506Z"
}
---
Our elasticsearch cluster failed. More information is available at [https://gitlab.com/minds/minds/issues/1092](https://gitlab.com/minds/minds/issues/1092). This issue is now resolved.

<!--- language code: en -->

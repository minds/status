---json
{
  "title": "High load on feeds",
  "date": "2020-05-14T08:19:55.987Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "web",
    "api",
    "boost",
    "search"
  ],
  "resolved": true,
  "modified": "2020-05-14T10:46:08.067Z"
}
---
Our ElasticSearch servers are currently experiencing high load and rejecting some requests.

<!--- language code: en -->

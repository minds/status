---json
{
  "title": "High server load",
  "date": "2020-05-20T14:12:59.565Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "web",
    "api"
  ],
  "resolved": true,
  "modified": "2020-05-21T11:59:31.545Z"
}
---
We are currently experiencing high load due to an influx of new users. We're currently expanding our capacity to deal with this.

<!--- language code: en -->

---json
{
  "title": "Users cannot log in via mobile app",
  "date": "2020-07-24T20:40:03.251Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "web",
    "api"
  ],
  "resolved": true,
  "modified": "2020-07-24T21:10:03.371Z"
}
---
It appears users cannot log in right now via mobile. We will provide updates when we are aware of the cause.

Update:
This appears to have been caused by a pod failing to read its configuration settings. We have restarted the container and are adding in additional health checks and other diagnostics - we will continue to monitor this issue.
<!--- language code: en -->

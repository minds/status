---json
{
  "title": "Site Outage",
  "date": "2021-01-20T17:26:35.449Z",
  "severity": "major-outage",
  "affectedsystems": [
    "web",
    "api"
  ],
  "resolved": true,
  "modified": "2021-01-20T18:20:22.512Z"
}
---
We are investigating a seperate outage to the sockets issues today, manifesting as high latency and 50x errors. Our team is working hard to resolve this issue.
You can track the issue [here](https://gitlab.com/minds/minds/-/issues/1898).
<!--- language code: en -->

---json
{
  "title": "Sockets container outage",
  "date": "2021-01-20T16:17:57.211Z",
  "severity": "partial-outage",
  "affectedsystems": [
    "live-updates"
  ],
  "resolved": true,
  "modified": "2021-01-26T16:57:35.265Z"
}
---
Our sockets servers are currently under heavy demand and as a result comments may not load live for all users intermitantly whilst we scale up our servers.
